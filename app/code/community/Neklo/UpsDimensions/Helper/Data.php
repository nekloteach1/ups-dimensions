<?php

class Neklo_UpsDimensions_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getBestPackage($dimensions)
    {
        $packages[] = $this->first($dimensions);
        $packages[] = $this->second($dimensions);
        
        $best = 9999;
        foreach ($packages as $key => $package){
            $test = $this->getCalculations($package);
            if($this->getCalculations($package) < $best){
                $best = $this->getCalculations($package);
                $method = $key;
            }
        }
        
        return $packages[$method];
    }
    
    protected function getCalculations($dimensions)
    {
        return $dimensions[0] + 2*$dimensions[1] + 2*$dimensions[2];
    }
    
    public function first($dimensions)
    {
        $currentHeight = 0;
        $iterator = 0;
        $emptyLevelSquare = array();
        $lastHeight = 0;
        foreach ($dimensions as $dimension) {
            
            if($iterator == 0){
                $emptyLevelSquare[0][0]['w'] = $dimension[0];
                $lastKey = end($emptyLevelSquare);
                $emptyLevelSquare[0][0]['l'] = $dimension[1];
                $lastHeight = $dimension[2];
                $currentHeight +=  $dimension[2];
                $iterator++;
                continue;
            }
            foreach ($emptyLevelSquare as $key => &$levels) {
                $flag = 0;
                if(count($levels) == 1){
                    $currentHeight += $dimension[2];
                    $lastHeight = $dimension[2];
                    $emptyLevelSquare[$key][0]['w'] -= $dimension[0];
                    
                    $emptyLevelSquare[$key][1]['w'] = $emptyLevelSquare[$key][0]['w'];
                    $emptyLevelSquare[$key][1]['l'] = $dimension[1];
                    
                    $emptyLevelSquare[$key+1][0]['w'] = $dimension[0];
                    $emptyLevelSquare[$key+1][0]['l'] = $dimension[1];
                    $key++;
                    $flag = 1;
                    break;
                }else{
                    foreach ($levels as $levelKey => &$item) {
                        if($item['w'] >= $dimension[0] AND $item['l'] >= $dimension[1]){
                            if($dimension[2] > $lastHeight){
                                $currentHeight = $currentHeight - $lastHeight + $dimension[2];
                            }
                            unset($emptyLevelSquare[$key]);
                            unset($levels[$levelKey]);
                            $flag = 1;
                            break;
                        }
                    }
                }
                if($flag == 1){
                    break;
                }
            }
            if($flag == 1){
                continue;
            }
            $currentHeight += $dimension[2];
            $lastHeight = $dimension[2];
            $emptyLevelSquare[$key][0]['w'] -= $dimension[0];
            
            $emptyLevelSquare[$key][1]['w'] -= $dimension[0];
            $emptyLevelSquare[$key][1]['l'] = $emptyLevelSquare[$key][0]['w'];
            $key++;
            
            
        }
        
        $result[0] = $dimensions[0][0];
        $result[1] = $dimensions[0][1];
        $result[2] = $currentHeight;
        rsort($result);
        return $result;
    }
    
    public function second($dimensions)
    {
        $result = array();
        for($i = 0; $i < count($dimensions); $i++){
            if($i > 0) {
                $result[2] += $dimensions[$i][2];
                rsort($result);
            }else{
                $result = $dimensions[$i];
            }
        }
        return $result;
    }
}