<?php

class Neklo_UpsDimensions_Model_Source_Config
{
    
    public function toOptionArray()
    {
        return array(
            array('value' => 'CM',
                  'label' => Mage::helper('neklo_upsdimensions')->__('CM')),
            array('value' => 'IN',
                  'label' => Mage::helper('neklo_upsdimensions')->__('IN')),
        );
    }
    
}