<?php

class Neklo_UpsDimensions_Model_Ups
    extends Mage_Usa_Model_Shipping_Carrier_Ups
{
    protected $dimensions = array();
   
    
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag($this->_activeFlag)) {
            return false;
        }
        $dimensions = array();
        foreach ($request->getAllItems() as $item) {
            if ($item->getProductType() == 'simple') {
                $productIds[] = $item->getProductId();
            }
        }
        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', array('in' => $productIds));
        foreach ($request->getAllItems() as $item => $value) {
            foreach ($products as $product) {
                if ($value->getProductId() == $product->getEntityId()) {
                    $prodDimensions = explode(
                        'x', $product->getUpsDimensions()
                    );
                    rsort($prodDimensions, SORT_NUMERIC);
                    for($i = 0; $i < $value->getQty(); $i++) {
                        $dimensions[] = $prodDimensions;
                    }
                }
            }
        }
        rsort($dimensions);
        $result = Mage::helper('neklo_upsdimensions')->getBestPackage($dimensions);
        
        $this->dimensions['length'] = $result[0];
        $this->dimensions['width'] = $result[1];
        $this->dimensions['height'] = $result[2];
        /*
         $this->dimensions['width'] = 15;//$result[0];
         $this->dimensions['height'] = 30;//$result[1];
         $this->dimensions['length'] = 40;//$result[2];
         */
        $this->setRequest($request);
        
        $this->_result = $this->_getQuotes();
        $this->_updateFreeMethodQuote($request);
        
        return $this->getResult();
    }
    
    /**
     * Get cgi rates
     *
     * @return Mage_Shipping_Model_Rate_Result
     */
    protected function _getCgiQuotes()
    {
        $r = $this->_rawRequest;
        
        $params = array(
            'accept_UPS_license_agreement' => 'yes',
            '10_action'      => $r->getAction(),
            '13_product'     => $r->getProduct(),
            '14_origCountry' => $r->getOrigCountry(),
            '15_origPostal'  => $r->getOrigPostal(),
            'origCity'       => $r->getOrigCity(),
            '19_destPostal'  => Mage_Usa_Model_Shipping_Carrier_Abstract::USA_COUNTRY_ID == $r->getDestCountry() ?
                substr($r->getDestPostal(), 0, 5) :
                $r->getDestPostal(),
            '22_destCountry' => $r->getDestCountry(),
            '23_weight'      => $r->getWeight(),
            '25_length'      => $this->dimensions['length'],
            '26_width'       => $this->dimensions['width'],
            '27_height'      => $this->dimensions['height'],
            '47_rate_chart'  => $r->getPickup(),
            '48_container'   => $r->getContainer(),
            '49_residential' => $r->getDestType(),
            'weight_std'     => 'kg',strtolower($r->getUnitMeasure()),
            'length_std'     => strtolower(Mage::getStoreConfig('carriers/ups/dimensions_unit')),
        
        );
        $params['47_rate_chart'] = $params['47_rate_chart']['label'];
        
        $responseBody = $this->_getCachedQuotes($params);
        if ($responseBody === null) {
            $debugData = array('request' => $params);
            try {
                $url = $this->getConfigData('gateway_url');
                if (!$url) {
                    $url = $this->_defaultCgiGatewayUrl;
                }
                $client = new Zend_Http_Client();
                $client->setUri($url);
                $client->setConfig(array('maxredirects'=>0, 'timeout'=>30));
                $client->setParameterGet($params);
                $response = $client->request();
                $responseBody = $response->getBody();
                
                $debugData['result'] = $responseBody;
                $this->_setCachedQuotes(x, $responseBody);
            }
            catch (Exception $e) {
                $debugData['result'] = array('error' => $e->getMessage(), 'code' => $e->getCode());
                $responseBody = '';
            }
            $this->_debug($debugData);
        }
        
        return $this->_parseCgiResponse($responseBody);
    }
}